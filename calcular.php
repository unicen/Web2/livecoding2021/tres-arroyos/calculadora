<?php

    require_once('lib/utils.php');
    require_once('lib/matematica.php'); 

    function verificarCompletos() {
        if (
            (isset($_POST["X"]) && ($_POST["X"] != null) && ($_POST["X"] != '')) &&
            (isset($_POST["Y"]) && ($_POST["Y"] != null) && ($_POST["Y"] != '')) &&
            (isset($_POST["operacion"]) && ($_POST["operacion"] != null) && ($_POST["operacion"] != ''))
        )
            return true;
        else
            return false;
    }

    mostrarEncabezado();
    mostrarMenu();
    mostrarFormulario();

    if (verificarCompletos()) 
    {
        $x = $_POST["X"];
        $y = $_POST["Y"];
        $operacion = $_POST["operacion"];
    
        switch ($operacion) {
            case "Sumar": sumar($x, $y); break;
            case "Restar": restar($x, $y); break;
            case "Dividir": dividir($x, $y); break;
            case "Multiplicar": multiplicar($x, $y); break;
            default: {
                    mostrarError("Operacion no definida");
                    die;
             } break;
        }
    } else {
        mostrarError("Falta algun parametro");
    }
     


    mostrarPie();
?>

