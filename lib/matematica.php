<?php
    require_once('lib/utils.php');
    
    function sumar($x, $y) {
        $resultado = $x + $y;
        mostrarMensaje ("El resultado de $x + $y es $resultado");
    }

    function restar($x, $y) {
        $resultado = $x - $y;
        mostrarMensaje ("El resultado de $x - $y es $resultado");
    }

    function multiplicar($x, $y) {
        $resultado = $x * $y;
        mostrarMensaje ("El resultado de $x * $y es $resultado");
    }

    function dividir($x, $y) {

        if ($y != 0) {
            $resultado = $x / $y;
            mostrarMensaje ("El resultado de $x / $y es $resultado");
        } else {
            mostrarError ("No es posible dividir por cero");
        }
    }

    function pi_n() {

        $n = 500000;
        $acum = 1;
        $signo = 1;
        for ($i = 1; $i < $n; $i++) {
            $signo = -$signo;
            $acum = $acum + $signo / (2 * $i +1);
        }
        $pi = $acum * 4;

        return $pi;
    }

?>