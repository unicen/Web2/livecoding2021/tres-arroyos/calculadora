"use strict"

let formCalc = document.querySelector("#form-calc");
let conteiner = document.querySelector("#conteiner");
formCalc.addEventListener('submit',enviar);

async function enviar(e){
    e.preventDefault();
    let formData = new FormData(this);
    let operacion = formData.get('operacion');
    let x = formData.get('X');
    let y = formData.get('Y');
    let url = operacion+'/'+x+'/'+y;
    let response = await fetch(url)
    let html = await response.text();
    conteiner.innerHTML = html;
}