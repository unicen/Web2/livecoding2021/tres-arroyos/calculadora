<?php

function mostrarEncabezado() {
    $salida = '<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

        <base href="http://localhost/proyectos/web2_2021/TA%202021/repo/calculadora/">
    </head>
    <body>
    <div class="container">';
    echo $salida;
        
}

function mostrarMenu() {
    $salida = '
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="home">Inicio</a>
                    <a class="nav-item nav-link" href="pi">Pi</a>
                    <a class="nav-item nav-link" href="about/Juan">Acerca de</a>
                </div>
            </div>

        </nav>
        ';
    echo $salida;
}

function mostrarFormulario() {
    $salida = ' <form id="form-calc" method="post">
                <label>X: <input type="number" name="X"></label>
                <label>Y: <input type="number" name="Y"></label>
                <label>Operacion: <select  name="operacion">
                                <option selected>--Seleccione--</option>
                                    <option>sumar</option>
                                    <option>restar</option>
                                    <option>dividir</option>
                                    <option>multiplicar</option>
                                </select> </label>
                <input type="submit" value="Calcular">
            </form>
            <div id=conteiner></div>
            <script src="lib/main.js"></script>
            ';
    echo $salida;
}

function mostrarPie() {
    $salida = '    
            </div>
        </body>
    </html>';
    echo $salida;
}


function mostrarError($mensaje) {
    $salida = '
    <div class="alert alert-danger">'.$mensaje.'</div>';
    echo $salida;
}

function mostrarMensaje($mensaje) {
    $salida = '
    <div class="alert alert-success">'.$mensaje.'</div>';
    echo $salida;
}