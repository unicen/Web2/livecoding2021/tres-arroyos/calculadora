<?php
require_once('lib/utils.php');
require_once('lib/matematica.php');
require_once('sections.php');

if (!empty($_GET['action'])){
    $accion = $_GET['action'];
}
else {
    $accion = 'home';
}

$params = explode('/',$accion);

switch ($params[0]){
    case 'home':
        showHome();
        break;
    case 'sumar':
        sumar($params[1],$params[2]);
        break;
    case 'restar':
        restar($params[1],$params[2]);
        break;
    case 'dividir':
        dividir($params[1],$params[2]);
        break;        
    case 'multiplicar':
        multiplicar($params[1],$params[2]);
        break;
    case 'about':
        showAbout($params[1]);
        break;
    case 'pi':
        showPi();
        break;
    default:
        echo('404 page not found');
        break;
}