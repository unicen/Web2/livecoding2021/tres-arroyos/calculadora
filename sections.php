<?php
    require_once('lib/utils.php');
    require_once('lib/matematica.php');    

    function showHome(){
    
        mostrarEncabezado();
        mostrarMenu();
        mostrarFormulario();
    
        mostrarPie();
    }

    function showPi(){
        mostrarEncabezado();
        mostrarMenu();
        mostrarFormulario();
    
        $pi = pi_n();
        mostrarMensaje("El valor de Pi es $pi");
    
        mostrarPie();
    }

    function showAbout($name = null){
        mostrarEncabezado();
        mostrarMenu();
        mostrarFormulario();
        if(!empty($name)){
            mostrarMensaje("El autor es $name");
        }
        else{
            mostrarError('falta el parametro el autor');
        }
        mostrarPie();
    }